var debug = require('debug')('app');

// cfenv provides access to your Cloud Foundry environment
// for more info, see: https://www.npmjs.com/package/cfenv
var cfenv = require('cfenv');
// get the app environment from Cloud Foundry
var appEnv = cfenv.getAppEnv();

var BOT_TOKEN = "295771464:AAHX-IEDkiY74apWUmkuxI6YqjhtMtFo7RQ";

var express = require('express');
var http = require('http');
var TelegramBot = require('node-telegram-bot-api');
var startHandler = require('./start');
var imageHandler = require('./image');
var linkHandler = require('./link');
//add other handlers

//Create Express app to serve client
var app = express();
//Create WebServer
var httpSrv = http.createServer(app);

//Serve public folder statically
app.use(express.static('public'));

//Listen
httpSrv.listen(appEnv.port, '0.0.0.0', function() {
  console.log('server starting on ' + appEnv.url);
});

function exitHandler(options, err) {
  if (options.cleanup) {
    debug('Quitting and Cleaning up');
  }
  if (err) {
    debug(err.stack);
  }
  if (options.exit) {
    debug('Exit');
    process.exit();
  }
}

//do something when app is closing
process.on('exit', exitHandler.bind(null,{cleanup: true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit: true}));

//catches uncaught exceptions
process.on('uncaughtException', function (exception) {
  var lines = exception.toString().match(/[^\r\n]+/g);

  lines.forEach(function (line) {
    console.log(line);
  });
});

// Setup polling way
var bot = new TelegramBot(BOT_TOKEN, {
  polling: true
});

// Matches /start
bot.onText(/\/start/, function(msg) {
  startHandler.handle(bot, msg, true);
});

bot.onText(/\/help/, function(msg) {
  startHandler.handle(bot, msg, false);
});

bot.on('message', function(msg) {
  var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
  var regex = new RegExp(expression);

  console.log("Got a message " + msg);

  if(msg.text && msg.text.match(regex)) {
    linkHandler.handleLink(bot, msg);
  }
  if(msg.photo != null) {
    imageHandler.handleImage(bot, msg);
  }
});
