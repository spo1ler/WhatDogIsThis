var classify = require('./classify_dog');
var strbr = require("./stringify_breeds");

module.exports.handleLink = function(bot, msg) {
  var imageLink = msg.text;

  bot.sendMessage(msg.from.id, "Let me think");

  classify.classifyLink(imageLink, function(res, err) {
    if(err) {
      console.log("Link classification failed: " + err);
      bot.sendMessage(msg.from.id, "I don't know for sure, but something bad happened. Sorry.");
    }
    else {
      console.log("Link classification successful: " + JSON.stringify(res));
      //bot.sendMessage(msg.from.id, JSON.stringify(res));
      bot.sendMessage(msg.from.id, strbr.stringifyBreeds(res));
    }
  });
};
