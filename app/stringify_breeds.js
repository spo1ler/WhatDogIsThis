var classify = require("./classify_dog");

module.exports.stringifyBreeds = function(result) {
  if(result.isDog) {
    if(result.breeds.length > 0) {
      var sortedBreeds = result.breeds.sort(function(lhs, rhs) { return lhs.score - rhs.score; });
      var topBreed = sortedBreeds[0];
      var topBreedName = classify.breedName(topBreed.breedId);

      var runnerUp = undefined;
      var runnerUpBreedName = undefined;

      if(sortedBreeds.length > 1 && topBreed.score < 0.75) {
        if(sortedBreeds[1].score > 0.5) {
          runnerUp = sortedBreeds[1];
          runnerUpBreedName = classify.breedName(runnerUp);
        }
      }

      if(topBreed.score > 0.8) {
        return "I am sure this is a " + topBreedName;
      }
      else if(topBreed.score > 0.5) {
        if(runnerUp) {
          return "I am pretty sure this is a " + topBreedName + ", but it can also be " + runnerUpBreedName;
        }
        else {
          return "I am pretty sure this is a " + topBreedName;
        }
      }
      else {
        return "I'm not sure, but this can be " + topBreedName;
      }
    }
    else {
      return "It's a dog, but I don't know which one";
    }
  }
  else {
    return "Not a dog";
  }
}
