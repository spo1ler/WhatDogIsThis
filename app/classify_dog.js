var watson = require('watson-developer-cloud');

var VisualRecognitionV3 = require('watson-developer-cloud/visual-recognition/v3');

var DOG_CLASSIFIER_ID = "dogs_428633927";
var CLASSIFIER_IDS = ["default", DOG_CLASSIFIER_ID ];

var visual_recognition = new VisualRecognitionV3({
  api_key: 'c019d18163fe3474f0667040bcae7eab92b3d095',
  version_date: '2016-05-20'
});

var breeds = {
  "goldenretriever": "Golden Retriever",
  "husky": "Siberian Husky",
  "beagle": "Beagle"
};

var classifierPredicate = function(element) {
  return element.classifier_id == DOG_CLASSIFIER_ID;
}

var classToBreed = function(klass) {
  return {
    breedId: klass.class,
    score: klass.score
  };
};

var classify = function (params, callback) {
  console.log("Classifying image");
  visual_recognition.classify(params, function(err, res) {
    if (err) {
      console.log("Visual recognition service returned an error: " + err);
      callback(null, {error: err});
    }
    else {
      console.log("Visual recognition service responded: " + res);
      var imageResult = res.images[0];
      var imageClassifiers = imageResult.classifiers;
      console.log("Looking for dog classifier: " + res);
      var dogClassifierIndex = imageClassifiers.findIndex(classifierPredicate);
      if(dogClassifierIndex == -1)
      {
        console.log("Not a dog");
        callback({
          breeds: [],
          isDog: false
        });
      }
      else {
        console.log("A dog");
        var dogClassifier = imageClassifiers[dogClassifierIndex];
        var breedScores = dogClassifier.classes.map(classToBreed);
        callback({
           breeds: breedScores,
           isDog: true
         });
      }
    }
  });
};

module.exports.classifyLink = function(link, callback) {
  console.log("Classifying image by link");
  var params = {
    classifier_ids: CLASSIFIER_IDS,
    url: link
  };

  classify(params, callback);
};

module.exports.classifyImage = function(buffer, callback) {
  console.log("Classifying image");
  var params = {
    classifier_ids: CLASSIFIER_IDS,
    images_file: buffer
  };

  classify(params, callback);
};

module.exports.breedName = function(classId) {
  return breeds[classId];
}
