var classify = require('./classify_dog');
var strbr = require("./stringify_breeds");
var os = require("os");
var fs = require("fs");

module.exports.handleImage = function(bot, msg) {
  console.log("Handling image");

  var photo = msg.photo[0];

  console.log("Downloading image");
  bot.downloadFile(photo.file_id, os.tmpdir()).then(function(filename) {
    console.log("Downloaded image");
    var filestream = fs.createReadStream(filename);

    bot.sendMessage(msg.from.id, "Let me think");

    classify.classifyImage(filestream, function(res, err) {
      if(err) {
        console.log("Image classification failed: " + err);
        bot.sendMessage(msg.from.id, "I don't know for sure, but something bad happened. Sorry.");
      }
      else {
        console.log("Image classified successfuly: JSON.stringify(res)");
        //bot.sendMessage(msg.from.id, JSON.stringify(res));
        bot.sendMessage(msg.from.id, strbr.stringifyBreeds(res));
      }
    });
  });
};
