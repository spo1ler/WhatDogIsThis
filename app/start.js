var debug = require('debug')('start');

module.exports.handle = function(bot, msg, greet) {
    debug('%j', msg);
    if(greet) {
        bot.sendMessage(msg.from.id, 'Hello ' + msg.from.first_name + '. ');
    };

    bot.sendMessage(msg.from.id, 'Send me a picture of a dog or a link to an image and I will tell you what breed the dog is');
};
